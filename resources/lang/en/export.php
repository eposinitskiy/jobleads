<?php

return [
    'format' => [
        'csv'     => 'CSV Format',
        'xml'     => 'XML Format',
        'xml-cut' => 'XML with cut description',
    ],
];