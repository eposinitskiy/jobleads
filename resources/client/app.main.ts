import '@style/app';
import 'core-js/shim';
import 'reflect-metadata';
import '@img/arrow.svg';
import '@img/arrowCircle.svg';
import '@img/cross.svg';
import '@img/logo_JL.svg';
import '@img/mood.jpg';
import '@img/mood-mobile.jpg';

import {VueKernel} from "./bundles/app/vue.kernel";
import {AppBundle} from './bundles/app/app.bundle';
import {ToggleBundle} from './bundles/toggle/toggle.bundle';

export default VueKernel
    .boot(
        ToggleBundle,
        AppBundle,
    )
    .run('#app');
