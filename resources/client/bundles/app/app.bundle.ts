import {Container} from 'inversify';

import {IBundle, IComponentsProviderBundle} from '@app/bundles/app/bundle.manager';
import {AsyncComponent, Component} from "vue";
import {ScrollableListComponent} from "@app/bundles/app/components/ScrollableListComponent";

export class AppBundle implements IBundle, IComponentsProviderBundle {

    public static readonly components = {
        'scrollable-list': ScrollableListComponent
    };

    public bind(container: Container): void {
    }

    components(): { [p: string]: Component<any, any, any, any> | AsyncComponent<any, any, any, any> } {
        return AppBundle.components;
    }

}
