import Vue from 'vue';
import {Component, Prop} from 'vue-property-decorator';
import {HeadHunterItemTo} from "@app/bundles/app/components/HeadHunterItemTo";

@Component
export class ScrollableListComponent extends Vue {

    @Prop({required: false, default: 3})
    public limit: number;

    @Prop({required: true})
    public step: number;

    protected items: HeadHunterItemTo[];

    protected current: number = 0;

    protected top: number = 0;

    protected height: number;

    public list(): HeadHunterItemTo[] {
        return this.items;
    }

    public next(): void {
        const next = this.current + this.limit;

        if (next >= this.items.length) {
            return;
        }

        this.setTop(next);
    }

    public prev(): void {
        const prev = this.current - this.limit;

        if (prev <= 0) {
            this.setTop(0);

            return;
        }

        this.setTop(prev);
    }

    protected created(): void {
        this.items = this.createProfiles();
        this.height = (this.step * this.limit);
    }

    private createProfiles(): HeadHunterItemTo[] {
        return [
            new HeadHunterItemTo(
                1,
                'https://upload.wikimedia.org/wikipedia/commons/d/dc/Profile-jose-bermudez.jpg',
                'Jose Bermidez',
                'Test company 1',
                '15 Apr 2013'
            ),
            new HeadHunterItemTo(
                2,
                'https://images.unsplash.com/profile-1528434786095-85c9344889b0?dpr=2&auto=format&fit=crop&w=128&h=128&q=60&crop=faces&bg=fff',
                'Timothy Choy',
                'Test company 2',
                '12 Apr 2013'
            ),
            new HeadHunterItemTo(
                3,
                'https://avatars3.githubusercontent.com/u/113001?s=460&v=4',
                'Aaron Parecki',
                'Test company 3',
                '10 Apr 2013'
            ),
            new HeadHunterItemTo(
                4,
                'https://hvbiz.com/profile/image/profile_image/851/xxlarge/crop=auto/_v=1456938279',
                'YULIA OVCHINNIKOVA',
                'Test company 4',
                '8 Apr 2013'
            ),
            new HeadHunterItemTo(
                5,
                'https://static.intercomassets.com/avatars/1818451/square_128/profile-sofia-Bennett1-1518521451.jpg?1518521451',
                'Test Headhunter',
                'Test company 5',
                '5 Apr 2013'
            ),
        ];
    }

    private setTop(steps: number): void {
        this.current = steps;
        this.top = -1 * (steps * this.step);
    }
}