export class HeadHunterItemTo {
    public id: number;
    public image: string;
    public name: string;
    public company: string;
    public date: string;

    constructor(id: number, image: string, name: string, company: string, date: string) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.company = company;
        this.date = date;
    }
}