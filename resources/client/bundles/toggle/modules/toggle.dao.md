## Toggle DAO

### Description
Data access object which can interact with existing toggle states from scripts.

### Details
- __Inject:__ `toggleBundleType.Dao`.

### Methods
- `attach(name: string, visible: boolean): void` - Method can initialize toggle state with identifier (_name_) and initial state value (_visible_). Used internally by __Toggle Container Component__. Try to avoid to use it manually or do it with care;
- `show(...names: string[]): void` - Method sets state `shown`/`on` for all toggles mentioned in names collection. The new named toggle state will be initialized as `shown`/`on` if it doesn't exist;
- `hide(...names: string[]): void` - Method sets state `hidden`/`off` for all toggles mentioned in names collection. The new named toggle state will be initialized as `hidden`/`off` if it doesn't exist;
- `toggle(...names: string[]): void` - Method sets state to opposite value for all toggles mentioned in names collection. All mentioned toggle states must exist, otherwise an exception will be thrown;
- `isSomeVisible(...names: string[]): boolean` - Method can define if __at least one__ of mentioned toggle states have `shown`/`on` state;
- `isEveryVisible(...names: string[]): boolean` - Method can define if __all__ mentioned toggle states have `shown`/`on` state;
- `isRegistered(...names: string[]): boolean` - Method can define if __all__ mentioned toggle states are exist.

### Examples
- __Toggle containers from service:__
```js
import { inject, injectable } from 'inversify';

import { toggleBundleType } from '@toggle/toggle.bundle.type';

@injectable()
export class ExampleService {

    constructor(@inject(toggleBundleType.Dao) toggleDao) {
        this.toggleDao = toggleDao;
    }

    toggleContainers(...toggleNames) {
        if (!this.toggleDao.isRegistered(...toggleNames)) return;
        this.toggleDao.toggle(...toggleNames);
    }

}
```
