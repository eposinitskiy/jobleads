import '@style/hh';
import 'core-js/shim';
import 'reflect-metadata';
import '@img/logo_JL.svg';

import {VueKernel} from "./bundles/app/vue.kernel";
import {AppBundle} from './bundles/app/app.bundle';
import {ToggleBundle} from './bundles/toggle/toggle.bundle';

export default VueKernel
    .boot(
        ToggleBundle,
        AppBundle,
    )
    .run('#app');
