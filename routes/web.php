<?php

use App\Export\Http\Controller\ExportController;
use App\Http\Controllers\Headhunters;
use Illuminate\Support\Facades\Route;

Route::get('/export', sprintf('%s@%s', ExportController::class, 'form'));
Route::post('/export', sprintf('%s@%s', ExportController::class, 'export'))
     ->name('export.post');

Route::get('/hh', sprintf('%s@%s', Headhunters::class, 'index'));
