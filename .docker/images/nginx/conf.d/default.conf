server {
    listen 0.0.0.0:80 default;
    listen [::]:80 default;

    root /var/www/app/public;
    index index.php index.html;

    server_name _;

    gzip on;
    gzip_proxied any;
    gzip_disable "msie6";
    gzip_types
        text/css
        text/plain
        text/javascript
        application/javascript
        application/json
        application/hal+json
        application/x-javascript
        application/xml
        application/xml+rss
        application/xhtml+xml
        application/x-font-ttf
        application/x-font-opentype
        application/vnd.ms-fontobject
        image/svg+xml
        image/x-icon
        application/rss+xml
        application/atom_xml;
    gzip_comp_level 5;
    gzip_vary on;
    gzip_min_length 50;

    location ~ \.map$ {
        try_files $uri =404;
        root /var/www/app;
    }

    location ~* \.(jpg|jpeg|gif|png|css|js|ico|xml|html)$ {
        try_files         $uri $uri/ /index.php?$query_string;
        access_log        off;
        log_not_found     off;
        expires           1d;
    }

    location / {
        client_max_body_size 10m;
        client_body_buffer_size 128k;
        try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        resolver 127.0.0.11 ipv6=off valid=5s;

        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass app:9000;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}
