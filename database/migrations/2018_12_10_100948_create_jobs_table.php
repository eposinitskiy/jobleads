<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->text('description');
            $table->string('company', 255);
            $table->timestamps();
        });

        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 30; $i++) {
            DB::table('jobs')->insert(
                [
                    'name'        => $faker->words(3, true),
                    'description' => $faker->text,
                    'company'     => $faker->company,
                ]
            );
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
