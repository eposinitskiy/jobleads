<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AddStudentsAndGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->timestamps();
        });

        Schema::create('classes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('students_grades', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('student_id');
            $table->unsignedInteger('class_id');
            $table->tinyInteger('score');

            $table->foreign('student_id')
                  ->references('id')
                  ->on('students')
                  ->onDelete('cascade');

            $table->foreign('class_id')
                  ->references('id')
                  ->on('classes')
                  ->onDelete('cascade');

            $table->timestamps();
        });

        $names    = ['Mike', 'Luke', 'George', 'Arthur'];
        $surnames = ['Meyers', 'Skywalker', 'Lucas', 'Lindt'];
        $students = [];

        for ($i = 0; $i < 10; $i++) {
            $students[] = DB::table('students')->insertGetId(
                [
                    'first_name' => collect($names)->random(),
                    'last_name'  => collect($surnames)->random(),
                ]
            );
        }

        $classes = [];
        foreach (['Chemistry', 'Maths', 'Literature', 'Biology'] as $item) {
            $classes[] = DB::table('classes')->insertGetId(['name' => $item]);
        }

        foreach ($students as $studentId) {
            foreach ($classes as $classId) {
                DB::table('students_grades')->insert(
                    [
                        'student_id' => $studentId,
                        'class_id'   => $classId,
                        'score'      => random_int(1, 5),
                    ]
                );
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
