<?php

use App\Export\Formatter\CsvFormatter;
use App\Export\Formatter\XmlCutFormatter;
use App\Export\Formatter\XmlFormatter;

return [
    'formats' => [
        'csv'     => CsvFormatter::class,
        'xml'     => XmlFormatter::class,
        'xml-cut' => XmlCutFormatter::class,
    ],
];