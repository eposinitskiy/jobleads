<?php

namespace App\TaxManager\Report;

/**
 * Interface RateProvider
 *
 * @package App\TaxManager\Report
 */
interface RateProvider
{
    public function rate(): float;
}