<?php

namespace App\TaxManager\Report\Support;

use App\TaxManager\Report\IncomeProvider;

/**
 * Trait CollectsIncome
 *
 * @package App\TaxManager\Report\Support
 */
trait CollectsIncome
{
    /**
     * @param IncomeProvider[] $providers
     *
     * @return mixed
     */
    protected function collectIncome(array $providers)
    {
        return collect($providers)->sum(function (IncomeProvider $provider) {
            return $provider->income();
        });
    }
}