<?php

namespace App\TaxManager\Report\Support;

use InvalidArgumentException;

/**
 * Trait ProviderGuard
 *
 * @package App\TaxManager\Report\Support
 */
trait ProviderGuard
{
    /**
     * @param mixed  $subject
     * @param string $expected
     */
    protected function ensureInstanceOf($subject, string $expected): void
    {
        if ($subject instanceof $expected) {
            return;
        }

        throw new InvalidArgumentException(
            sprintf(
                'Instance of [%s] expected, got [%s]',
                $expected,
                \is_object($subject) ? \get_class($subject) : \gettype($subject)
            )
        );
    }

    /**
     * @param array  $subjects
     * @param string $expected
     */
    protected function ensureContainsInstanceOf(array $subjects, string $expected): void
    {
        foreach ($subjects as $subject) {
            if ($subject instanceof $expected) {
                continue;
            }

            $this->ensureInstanceOf($subject, $expected);
        }
    }
}