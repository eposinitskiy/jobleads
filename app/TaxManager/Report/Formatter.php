<?php

namespace App\TaxManager\Report;

/**
 * Class Formatter
 *
 * @package App\TaxManager\Report
 */
class Formatter
{
    /**
     * @var int
     */
    protected $decimal;

    /**
     * @var string
     */
    protected $decimalPoint;

    /**
     * @var string
     */
    protected $thousandSeparator;

    /**
     * Formatter constructor.
     *
     * @param int    $decimal
     * @param string $decimalPoint
     * @param string $thousandSeparator
     */
    public function __construct(int $decimal = 2, string $decimalPoint = '.', string $thousandSeparator = ' ')
    {
        $this->decimal           = $decimal;
        $this->decimalPoint      = $decimalPoint;
        $this->thousandSeparator = $thousandSeparator;
    }

    /**
     * @param float $number
     *
     * @return string
     */
    public function format(float $number): string
    {
        return number_format($number, $this->decimal, $this->decimalPoint, $this->thousandSeparator);
    }
}