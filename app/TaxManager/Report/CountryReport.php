<?php

namespace App\TaxManager\Report;

use App\TaxManager\Report\Support\CollectsIncome;
use App\TaxManager\Subject\StateSubject;

/**
 * Class CountryReport
 *
 * @package App\TaxManager\Report
 */
class CountryReport
{
    use CollectsIncome;

    /**
     * @var Processor
     */
    protected $processor;

    /**
     * @var Formatter
     */
    protected $formatter;

    /**
     * @var StateSubject[]
     */
    protected $states = [];

    /**
     * @var array
     */
    protected $rates = [];

    /**
     * @var array
     */
    protected $income = [];

    /**
     * CountryReport constructor.
     *
     * @param Processor $processor
     * @param Formatter $formatter
     */
    public function __construct(Processor $processor, Formatter $formatter)
    {
        $this->processor = $processor;
        $this->formatter = $formatter;
    }

    /**
     * @param StateSubject $state
     */
    public function add(StateSubject $state): void
    {
        $id = md5($state->getName());

        $this->states[$id] = $state;
        $this->rates[$id]  = $state->getCounties();
        $this->income[$id] = $state->getCounties();
    }

    /**
     * @return array
     */
    public function statesIncome(): array
    {
        $report = [];
        foreach ($this->states as $state) {
            $report[$state->getName()] = $this->formatter->format($state->income());
        }

        return $report;
    }

    /**
     * @return array
     */
    public function statesAvgIncome(): array
    {
        $report = [];

        foreach ($this->income as $id => $incomes) {
            if (!($state = $this->states[$id] ?? false)) {
                unset($this->income[$id]);
                continue;
            }

            $report[$state->getName()] = $this->formatter->format($this->processor->avgIncome($incomes));
        }

        return $report;
    }

    /**
     * @return array
     */
    public function statesAvgRate(): array
    {
        $report = [];

        foreach ($this->rates as $id => $rates) {
            if (!($state = $this->states[$id] ?? false)) {
                unset($this->rates[$id]);
                continue;
            }

            $report[$state->getName()] = $this->formatter->format($this->processor->avgRate($rates));
        }

        return $report;
    }

    /**
     * @return string
     */
    public function avgRate(): string
    {
        return $this->formatter->format($this->processor->avgRate(collect($this->rates)->flatten()->all()));
    }

    /**
     * @return string
     */
    public function totalIncome(): string
    {
        return $this->formatter->format($this->collectIncome($this->states));
    }
}