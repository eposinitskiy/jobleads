<?php

namespace App\TaxManager\Report;

/**
 * Interface IncomeProvider
 *
 * @package App\TaxManager\Report
 */
interface IncomeProvider
{
    public function income(): float;
}