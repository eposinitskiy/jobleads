<?php

namespace App\TaxManager\Report;

use App\TaxManager\Report\Support\ProviderGuard;

/**
 * Class Processor
 *
 * @package App\TaxManager\Report
 */
class Processor
{
    use ProviderGuard;

    /**
     * @param IncomeProvider[] $providers
     *
     * @return float
     */
    public function avgIncome(array $providers): float
    {
        $this->ensureContainsInstanceOf($providers, IncomeProvider::class);

        if (empty($providers)) {
            return 0;
        }

        $total = collect($providers)->sum(function (IncomeProvider $provider) {
            return $provider->income();
        });

        return $total / \count($providers);
    }

    /**
     * @param RateProvider[] $providers
     *
     * @return float
     */
    public function avgRate(array $providers): float
    {
        $this->ensureContainsInstanceOf($providers, RateProvider::class);

        if (empty($providers)) {
            return 0;
        }

        $total = collect($providers)->sum(function (RateProvider $provider) {
            return $provider->rate();
        });

        return $total / \count($providers);
    }
}