<?php

namespace App\TaxManager\Dto;

/**
 * Class TaxTo
 *
 * @package App\TaxManager\Dto
 */
class TaxTo
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var float
     */
    protected $rate = 0;

    /**
     * @var float
     */
    protected $income = 0;

    /**
     * TaxTo constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getRate(): float
    {
        return $this->rate;
    }

    /**
     * @param float $rate
     *
     * @return TaxTo
     */
    public function setRate(float $rate): TaxTo
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * @return float
     */
    public function getIncome(): float
    {
        return $this->income;
    }

    /**
     * @param float $income
     *
     * @return TaxTo
     */
    public function setIncome(float $income): TaxTo
    {
        $this->income = $income;

        return $this;
    }
}