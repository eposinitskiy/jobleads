<?php

namespace App\TaxManager\Subject;

use App\TaxManager\Report\IncomeProvider;
use App\TaxManager\Report\Support\CollectsIncome;

/**
 * Class StateTo
 *
 * @package App\TaxManager\Subject
 */
class StateSubject implements IncomeProvider
{
    use CollectsIncome;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var CountySubject[]
     */
    protected $counties = [];

    /**
     * StateSubject constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param CountySubject $county
     */
    public function add(CountySubject $county): void
    {
        $this->counties[] = $county;
    }

    /**
     * @return CountySubject[]
     */
    public function getCounties(): array
    {
        return $this->counties;
    }

    /**
     * @return float
     */
    public function income(): float
    {
        return $this->collectIncome($this->counties);
    }

}