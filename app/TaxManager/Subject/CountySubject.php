<?php

namespace App\TaxManager\Subject;

use App\TaxManager\Dto\TaxTo;
use App\TaxManager\Report\IncomeProvider;
use App\TaxManager\Report\RateProvider;

/**
 * Class CountySubject
 *
 * @package App\TaxManager\Subject
 */
class CountySubject implements IncomeProvider, RateProvider
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var TaxTo[]
     */
    protected $taxes = [];

    /**
     * CountySubject constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @param TaxTo $tax
     */
    public function addTax(TaxTo $tax): void
    {
        $taxId = md5($tax->getName());

        $this->taxes[$taxId] = $tax;
    }

    /**
     * @return TaxTo[]
     */
    public function getTaxes(): array
    {
        return $this->taxes;
    }

    /**
     * @return float
     */
    public function income(): float
    {
        $income = 0;

        foreach ($this->taxes as $tax) {
            $income += $tax->getIncome();
        }

        return $income;
    }

    /**
     * @return float
     */
    public function rate(): float
    {
        $rate = 0;

        foreach ($this->taxes as $tax) {
            $rate += $tax->getRate();
        }

        return $rate;
    }
}