<?php

namespace App\TaxManager\Repository;

use App\Model\County;
use App\Model\State;
use App\TaxManager\Dto\TaxTo;
use App\TaxManager\Subject\CountySubject;
use App\TaxManager\Subject\StateSubject;

/**
 * Class StateRepository
 *
 * @package App\TaxManager\Repository
 */
class StateRepository
{
    /**
     * @return StateSubject[]
     */
    public function states(): array
    {
        $states = State::with(['counties.taxes'])->get();

        return $states->map(function (State $state) {
            $stateSubject = new StateSubject($state->name);
            $state->counties()->each(function (County $county) use ($stateSubject) {
                $countySubject = new CountySubject($county->name);
                $stateSubject->add($countySubject);
                $county->taxes()->each(function ($tax) use ($countySubject) {
                    $taxDto = new TaxTo($tax->name);
                    $taxDto->setIncome($tax->pivot->income);
                    $taxDto->setRate($tax->pivot->rate);

                    $countySubject->addTax($taxDto);
                });
            });

            return $stateSubject;
        })->all();
    }
}