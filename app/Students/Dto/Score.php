<?php

namespace App\Students\Dto;

/**
 * Class Score
 *
 * @package App\Students\Dto
 */
class Score
{
    /**
     * @var StudentTo
     */
    protected $student;

    /**
     * @var int
     */
    protected $score = 0;

    /**
     * Score constructor.
     *
     * @param StudentTo $student
     * @param int       $score
     */
    public function __construct(StudentTo $student, int $score)
    {
        $this->student = $student;
        $this->score   = $score;
    }

    /**
     * @return StudentTo
     */
    public function getStudent(): StudentTo
    {
        return $this->student;
    }

    /**
     * @return int
     */
    public function getScore(): int
    {
        return $this->score;
    }
}