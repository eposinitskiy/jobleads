<?php

namespace App\Students\Dto;

/**
 * Class StudyClassTo
 *
 * @package App\Students\Dto
 */
class StudyClassTo
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Score[]
     */
    protected $scores = [];

    /**
     * StudyClassTo constructor.
     *
     * @param int    $id
     * @param string $name
     */
    public function __construct(int $id, string $name)
    {
        $this->id   = $id;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Score[]
     */
    public function getScores(): array
    {
        return $this->scores;
    }

    /**
     * @param Score $score
     */
    public function addScore(Score $score): void
    {
        $id = $score->getStudent()->getId();

        if (array_key_exists($id, $this->scores)) {
            return;
        }

        $this->scores[$id] = $score;
    }
}