<?php

namespace App\Students\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Student
 *
 * @package App\Students\Model
 *
 * @property string $first_name
 * @property string $last_name
 */
class Student extends Model
{
    protected $table = 'students';

    public function classes()
    {
        return $this->belongsToMany(
            StudyClass::class,
            'students_grades',
            'student_id',
            'class_id',
            'id',
            'id'
        )->withPivot(['score']);
    }
}