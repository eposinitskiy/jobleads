<?php

namespace App\Students\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class StudyClass
 *
 * @package App\Students\Model
 *
 * @property string $name
 */
class StudyClass extends Model
{

    protected $table = 'classes';

    /**
     * @return BelongsToMany
     */
    public function students(): BelongsToMany
    {
        return $this->belongsToMany(
            Student::class,
            'students_grades',
            'class_id',
            'student_id',
            'id',
            'id'
        )->withPivot(['score']);
    }
}