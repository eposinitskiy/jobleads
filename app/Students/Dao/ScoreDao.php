<?php

namespace App\Students\Dao;

use App\Students\Dto\Score;
use App\Students\Dto\StudentTo;
use App\Students\Dto\StudyClassTo;
use App\Students\Model\Student;
use App\Students\Model\StudyClass;
use App\Students\Subject\StudentSubject;
use App\Students\Subject\StudyClassSubject;

/**
 * Class ScoreDao
 *
 * @package App\Students\Dao
 */
class ScoreDao
{
    /**
     * @return StudentSubject[]
     */
    public function allStudents(): array
    {
        $students = Student::with('classes')->get();
        $result   = [];

        /** @var Student $student */
        foreach ($students as $student) {
            $studentTo      = new StudentTo($student->id, $student->first_name, $student->last_name);
            $studentSubject = new StudentSubject($studentTo);

            foreach ($student->classes as $class) {
                $studentSubject->addScore(
                    new StudyClassTo($class->id, $class->name),
                    new Score($studentTo, $class->pivot->score)
                );
            }

            $result[] = $studentSubject;
        }

        return $result;
    }

    /**
     * @return StudyClassSubject[]
     */
    public function allClasses(): array
    {
        $classes = StudyClass::with('students')->get();
        $result  = [];

        /** @var StudyClass $class */
        foreach ($classes as $class) {
            $studyClass = new StudyClassTo($class->id, $class->name);

            foreach ($class->students as $student) {
                $studyClass->addScore(
                    new Score(
                        new StudentTo($student->id, $student->first_name, $student->last_name), $student->pivot->score
                    )
                );
            }

            $result[] = new StudyClassSubject($studyClass);
        }

        return $result;
    }
}