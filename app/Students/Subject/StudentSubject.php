<?php

namespace App\Students\Subject;

use App\Students\Dto\Score;
use App\Students\Dto\StudentTo;
use App\Students\Dto\StudyClassTo;

/**
 * Class StudentSubject
 *
 * @package App\Students\Subject
 */
class StudentSubject implements ProvidesAvgScore
{
    /**
     * @var StudentTo
     */
    protected $student;

    /**
     * @var array
     */
    protected $scores = [];

    /**
     * StudentSubject constructor.
     *
     * @param StudentTo $student
     */
    public function __construct(StudentTo $student)
    {
        $this->student = $student;
    }

    /**
     * @return StudentTo
     */
    public function getStudent(): StudentTo
    {
        return $this->student;
    }

    /**
     * @return array
     */
    public function getScores(): array
    {
        return $this->scores;
    }

    /**
     * @param StudyClassTo $classTo
     * @param Score        $score
     */
    public function addScore(StudyClassTo $classTo, Score $score): void
    {
        $id = $classTo->getId();

        if (array_key_exists($id, $this->scores)) {
            return;
        }

        $this->scores[$id] = $score;
    }

    /**
     * @return float
     */
    public function avgScore(): float
    {
        $scores = collect($this->scores);

        if ($scores->isEmpty()) {
            return 0;
        }

        return $scores->sum(function (Score $score) {
                return $score->getScore();
            }) / $scores->count();
    }

}