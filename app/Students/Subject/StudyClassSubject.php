<?php

namespace App\Students\Subject;

use App\Students\Dto\Score;
use App\Students\Dto\StudyClassTo;

/**
 * Class StudyClassSubject
 *
 * @package App\Students\Subject
 */
class StudyClassSubject implements ProvidesAvgScore
{
    /**
     * @var StudyClassTo
     */
    protected $studyClass;

    /**
     * StudyClassSubject constructor.
     *
     * @param StudyClassTo $studyClass
     */
    public function __construct(StudyClassTo $studyClass)
    {
        $this->studyClass = $studyClass;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->studyClass->getName();
    }

    /**
     * @return float
     */
    public function avgScore(): float
    {
        $scores = collect($this->studyClass->getScores());

        if ($scores->isEmpty()) {
            return 0;
        }

        return $scores->sum(function (Score $score) {
                return $score->getScore();
            }) / $scores->count();
    }
}