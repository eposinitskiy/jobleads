<?php

namespace App\Students\Subject;

/**
 * Interface ProvidesAvgScore
 *
 * @package App\Students\Subject
 */
interface ProvidesAvgScore
{
    public function avgScore(): float;
}