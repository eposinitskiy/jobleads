<?php

namespace App\Http\Controllers;

/**
 * Class Headhunters
 *
 * @package App\Http\Controllers
 */
class Headhunters extends Controller
{
    public function index()
    {
        return view('hh/index');
    }
}