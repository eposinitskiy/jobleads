<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class County
 *
 * @package App\Model
 */
class County extends Model
{
    protected $table = 'counties';

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id', 'id');
    }

    public function taxes()
    {
        return $this->belongsToMany(
            Tax::class,
            'county_taxes',
            'county_id',
            'tax_id',
            'id',
            'id'
        )->withPivot(['rate', 'income']);
    }
}