<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Tax
 *
 * @package App\Model
 */
class Tax extends Model
{
    protected $table = 'taxes';
}