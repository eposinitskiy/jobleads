<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class State
 *
 * @package App\Model
 */
class State extends Model
{
    protected $table = 'states';

    public function counties()
    {
        return $this->hasMany(County::class, 'state_id', 'id');
    }
}