<?php

namespace App\Export\Report;

use App\Export\Command\ReportCreated;
use App\Export\Criteria\ExportCriteria;
use App\Export\Dto\Report;
use App\Export\Report\Pipes\EmailPipe;
use Illuminate\Pipeline\Pipeline;

/**
 * Class ReportManager
 *
 * @package App\Export\Report
 */
class ReportManager
{
    /**
     * @var ExportManager
     */
    protected $exportManager;

    /**
     * @var Pipeline
     */
    protected $pipeline;

    /**
     * ReportManager constructor.
     *
     * @param ExportManager $exportManager
     * @param Pipeline      $pipeline
     */
    public function __construct(ExportManager $exportManager, Pipeline $pipeline)
    {
        $this->exportManager = $exportManager;
        $this->pipeline      = $pipeline;
    }

    public function email(ExportCriteria $criteria, string $email): Report
    {
        return $this->pipeline->send($criteria)
                              ->through(
                                  [
                                      $this->reportPipe(),
                                      function (Report $report, callable $next) use ($email) {
                                          return $next(new ReportCreated($report, $email));
                                      },
                                      EmailPipe::class,
                                  ]
                              )
                              ->then(function (ReportCreated $command) {
                                  return $command->getReport();
                              });
    }

    protected function reportPipe(): callable
    {
        return function (ExportCriteria $criteria, callable $next) {
            $report = $this->exportManager->withCriteria($criteria)->getReport($criteria->format);

            return $next($report);
        };
    }
}