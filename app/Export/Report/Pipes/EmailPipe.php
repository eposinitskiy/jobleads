<?php

namespace App\Export\Report\Pipes;

use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Mail\Mailer;

/**
 * Class EmailPipe
 *
 * @package App\Export\Report\Pipes
 */
class EmailPipe
{
    /**
     * @var Mailer
     */
    protected $mailer;

    /**
     * EmailPipe constructor.
     *
     * @param Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param Mailable $report
     * @param callable $next
     *
     * @return mixed
     */
    public function __invoke(Mailable $report, callable $next)
    {
        $this->mailer->send($report);

        return $next($report);
    }
}