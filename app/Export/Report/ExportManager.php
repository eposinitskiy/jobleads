<?php

namespace App\Export\Report;

use App\Export\Criteria\ExportCriteria;
use App\Export\Dao\JobsDao;
use App\Export\Dto\JobsCollection;
use App\Export\Dto\Report;
use App\Export\Formatter\FormatterInterface;

/**
 * Class ExportManager
 *
 * @package App\Export
 */
class ExportManager
{
    /**
     * @var FormatterInterface[]
     */
    protected $formatters = [];

    /**
     * @var JobsDao
     */
    protected $dao;

    /**
     * @var ExportCriteria
     */
    protected $criteria;

    /**
     * ExportManager constructor.
     *
     * @param JobsDao $dao
     */
    public function __construct(JobsDao $dao)
    {
        $this->dao = $dao;
    }

    /**
     * @param string             $format
     * @param FormatterInterface $formatter
     */
    public function registerFormatter(string $format, FormatterInterface $formatter): void
    {
        $this->formatters[$format] = $formatter;
    }

    /**
     * @param ExportCriteria $criteria
     *
     * @return ExportManager
     */
    public function withCriteria(ExportCriteria $criteria): ExportManager
    {
        $this->criteria = $criteria;

        return $this;
    }

    /**
     * @param string $format OPTIONAL
     *
     * @return Report
     */
    public function getReport(?string $format = null): Report
    {
        return $this->format($this->deriveJobs(), $format);
    }

    /**
     * @TODO: PIP - file adapter could be used (i.e. Flysystem)
     *
     * @param string $filename
     * @param string $format OPTIONAL
     *
     * @return bool
     */
    public function toFile(string $filename, ?string $format = null): bool
    {
        $report = $this->getReport($format);

        try {
            file_put_contents($filename, $report->getContent());
        } catch (\Throwable $e) {
            // @TODO: do the logging

            return false;
        }

        return true;
    }

    /**
     * @return JobsCollection
     */
    protected function deriveJobs(): JobsCollection
    {
        $jobs = $this->dao->get($this->criteria);

        $this->criteria = null;

        return $jobs;
    }

    /**
     * @param JobsCollection $jobs
     * @param string|null    $format
     *
     * @return Report
     */
    protected function format(JobsCollection $jobs, ?string $format = null): Report
    {
        reset($this->formatters);

        $format = $format ?: key($this->formatters);

        if (!isset($this->formatters[$format])) {
            throw new \RuntimeException(
                sprintf('Format [%s] is not supported', $format)
            );
        }

        $formatter = $this->formatters[$format];

        return $formatter->format($jobs);
    }
}