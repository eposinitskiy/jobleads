<?php

namespace App\Export\Criteria;

use App\Dto\Attributes\AbstractAttributesObject;

/**
 * Class ExportCriteria
 *
 * @package App\Export\Criteria
 *
 * @property int    $limit
 * @property int    $offset
 * @property string $format
 */
class ExportCriteria extends AbstractAttributesObject
{
    /**
     * Returns available attribute names
     *
     * @return array
     */
    public static function attributes(): array
    {
        return [
            'format',
            'limit',
            'offset',
        ];
    }
}