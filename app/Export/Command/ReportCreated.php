<?php

namespace App\Export\Command;

use App\Export\Dto\Report;
use Illuminate\Mail\Mailable;

/**
 * Class ReportCreated
 *
 * @package App\Export\Command
 */
class ReportCreated extends Mailable
{
    /**
     * @var Report
     */
    protected $report;

    /**
     * @var string
     */
    protected $email;

    /**
     * ExportCreated constructor.
     *
     * @param Report $report
     * @param string $email
     */
    public function __construct(Report $report, string $email)
    {
        $this->report = $report;
        $this->email  = $email;
    }

    /**
     * @return Report
     */
    public function getReport(): Report
    {
        return $this->report;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @TODO: Naming service could be detached
     */
    public function build(): void
    {
        $this->to($this->email)
             ->subject('Jobs report created')
             ->view('mail/report-created-email')
             ->attachData(
                 $this->report->getContent(),
                 $this->report->filename(),
                 [
                     'mime' => $this->report->getMime(),
                 ]
             );
    }
}