<?php

namespace App\Export\Http\Controller;

use App\Export\Criteria\ExportCriteria;
use App\Export\Dto\Report;
use App\Export\Formatter\FormatterRegistry;
use App\Export\Http\Request\ExportRequest;
use App\Export\Report\ReportManager;
use App\Http\Controllers\Controller;

/**
 * Class ExportController
 *
 * @package App\Export\Http\Controller
 */
class ExportController extends Controller
{
    /**
     * @var FormatterRegistry
     */
    protected $formatterRegistry;

    /**
     * @var ReportManager
     */
    protected $reportManager;

    /**
     * ExportController constructor.
     *
     * @param FormatterRegistry $formatterRegistry
     * @param ReportManager     $reportManager
     */
    public function __construct(FormatterRegistry $formatterRegistry, ReportManager $reportManager)
    {
        $this->formatterRegistry = $formatterRegistry;
        $this->reportManager     = $reportManager;
    }

    public function form()
    {
        return view('export/form')->with('formats', $this->formatterRegistry->available());
    }

    public function export(ExportRequest $request)
    {
        $attributes = $request->getAttributes();

        $report = $this->reportManager->email(
            ExportCriteria::fromArray(['format' => $attributes->format]),
            $attributes->email
        );

        if ($report instanceof Report) {
            return view('export/ty')->with('email', $attributes->email);
        }

        return view('export/sorry');
    }
}