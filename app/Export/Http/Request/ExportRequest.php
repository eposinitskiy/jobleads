<?php

namespace App\Export\Http\Request;

use App\Export\Dto\Attributes\ExportRequestAttributes;
use App\Export\Formatter\FormatterRegistry;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * Class ExportRequest
 *
 * @package App\Export\Http\Request
 */
class ExportRequest extends FormRequest
{
    /**
     * @return array
     */
    public function rules(FormatterRegistry $registry)
    {
        return [
            'email'  => 'required|email',
            'format' => Rule::in($registry->available()),
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return ExportRequestAttributes
     */
    public function getAttributes()
    {
        return ExportRequestAttributes::fromArray($this->all());
    }
}