<?php

namespace App\Export\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Job
 *
 * @package App\Export\Model
 */
class Job extends Model
{
    protected $table = 'jobs';
}