<?php

namespace App\Export\Provider;

use App\Export\Dao\JobsDao;
use App\Export\Formatter\CsvFormatter;
use App\Export\Formatter\FormatterRegistry;
use App\Export\Formatter\XmlCutFormatter;
use App\Export\Formatter\XmlFormatter;
use App\Export\Report\ExportManager;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Class ExportProvider
 *
 * @package App\Export\Provider
 */
class ExportProvider extends ServiceProvider
{
    protected $defer = true;

    public function register()
    {
        $this->app->singleton(Serializer::class, function () {
            $encoders    = [new XmlEncoder()];
            $normalizers = [new ObjectNormalizer()];

            return new Serializer($normalizers, $encoders);
        });

        $this->app->singleton(XmlFormatter::class, function () {
            return new XmlFormatter($this->app->make(Serializer::class));
        });

        $this->app->singleton(XmlCutFormatter::class, function () {
            return new XmlCutFormatter($this->app->make(Serializer::class));
        });

        $this->app->singleton(CsvFormatter::class);

        $this->app->singleton(FormatterRegistry::class, function () {
            $registry = new FormatterRegistry();

            foreach (config('export.formats') as $format => $fqcn) {
                $registry->register($format, $this->app->make($fqcn));
            }

            return $registry;
        });

        $this->app->singleton(ExportManager::class, function () {
            $manager  = new ExportManager($this->app->make(JobsDao::class));
            $registry = $this->app->make(FormatterRegistry::class);

            foreach ($registry->all() as $format => $formatter) {
                $manager->registerFormatter($format, $formatter);
            }

            return $manager;
        });
    }

    public function provides()
    {
        return [
            Serializer::class,
            XmlFormatter::class,
            XmlCutFormatter::class,
            CsvFormatter::class,
            FormatterRegistry::class,
            ExportManager::class,
        ];
    }
}