<?php

namespace App\Export\Dao;

use App\Export\Criteria\ExportCriteria;
use App\Export\Dto\JobsCollection;
use App\Export\Dto\JobTo;
use App\Export\Model\Job;
use Illuminate\Database\Eloquent\Builder;

/**
 * Class JobsDao
 *
 * @package App\Export\Dao
 */
class JobsDao
{
    /**
     * @param ExportCriteria $criteria OPTIONAL
     *
     * @return JobsCollection
     */
    public function get(?ExportCriteria $criteria = null): JobsCollection
    {
        $query = Job::query();

        if ($criteria) {
            $this->applyCriteria($query, $criteria);
        }

        $collection = new JobsCollection();

        foreach ($query->get() as $item) {
            $collection->addJob(
                new JobTo($item->id, $item->name, $item->description, $item->company)
            );
        }

        return $collection;
    }

    /**
     * @TODO: PIP - More elegant way to apply criteria
     *
     * @param Builder        $query
     * @param ExportCriteria $criteria
     */
    protected function applyCriteria(Builder $query, ExportCriteria $criteria): void
    {
        if ($criteria->limit) {
            $query->limit($criteria->limit);
        }

        if ($criteria->offset) {
            $query->offset($criteria->offset);
        }
    }
}