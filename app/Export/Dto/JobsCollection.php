<?php

namespace App\Export\Dto;

use Traversable;

/**
 * Class JobsCollection
 *
 * @package App\Export\Dto
 */
class JobsCollection implements \Countable, \IteratorAggregate
{

    /**
     * @var JobTo[]
     */
    protected $jobs = [];

    /**
     * @param JobTo $job
     */
    public function addJob(JobTo $job): void
    {
        $id = $job->getId();

        if (array_key_exists($id, $this->jobs)) {
            return;
        }

        $this->jobs[$id] = $job;
    }

    /**
     * Retrieve an external iterator
     *
     * @link  https://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     * @since 5.0.0
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->jobs);
    }

    /**
     * Count elements of an object
     *
     * @link  https://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     * @since 5.1.0
     */
    public function count()
    {
        return \count($this->jobs);
    }
}