<?php

namespace App\Export\Dto;

/**
 * Class JobTo
 *
 * @package App\Export\Dto
 */
class JobTo
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $company;

    /**
     * JobTo constructor.
     *
     * @param int    $id
     * @param string $name
     * @param string $description
     * @param string $company
     */
    public function __construct(int $id, string $name, string $description, string $company)
    {
        $this->id          = $id;
        $this->name        = $name;
        $this->description = $description;
        $this->company     = $company;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getCompany(): string
    {
        return $this->company;
    }
}