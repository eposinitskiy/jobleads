<?php

namespace App\Export\Dto;

/**
 * Class Report
 *
 * @package App\Export\Dto
 */
class Report
{
    /**
     * @var string
     */
    protected $content;

    /**
     * @var string
     */
    protected $mime;

    /**
     * Report constructor.
     *
     * @param string $content
     * @param string $mime
     */
    public function __construct(string $content, string $mime)
    {
        $this->content = $content;
        $this->mime    = $mime;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getMime(): string
    {
        return $this->mime;
    }

    /**
     * @return string
     */
    public function filename(): string
    {
        $mimeParts = explode('/', $this->mime);

        return sprintf('jobs-report.%s', array_pop($mimeParts));
    }
}