<?php

namespace App\Export\Dto\Attributes;

use App\Dto\Attributes\AbstractAttributesObject;

/**
 * Class ExportRequestAttributes
 *
 * @package App\Export\Dto\Attributes
 *
 * @property string $email
 * @property string $format
 */
class ExportRequestAttributes extends AbstractAttributesObject
{

    /**
     * Returns available attribute names
     *
     * @return array
     */
    public static function attributes(): array
    {
        return [
            'email',
            'format',
        ];
    }
}