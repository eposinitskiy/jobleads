<?php

namespace App\Export\Formatter;

use App\Export\Dto\JobsCollection;
use App\Export\Dto\JobTo;
use App\Export\Dto\Report;
use League\Csv\CannotInsertRecord;
use League\Csv\Writer;

/**
 * Class CsvFormatter
 *
 * @package App\Export\Formatter
 */
class CsvFormatter implements FormatterInterface
{
    /**
     * @var string
     */
    protected $delimiter;

    /**
     * @TODO: PIP - more parameters for CSV output
     *
     * CsvFormatter constructor.
     *
     * @param string $delimiter
     */
    public function __construct(string $delimiter = ',')
    {
        $this->delimiter = $delimiter;
    }

    /**
     * @TODO: Potential improvement, to make it return streams for large files
     *
     * @param JobsCollection $collection
     *
     * @return Report
     * @throws CannotInsertRecord
     */
    public function format(JobsCollection $collection): Report
    {
        $writer = Writer::createFromFileObject(new \SplTempFileObject());
        $writer->insertOne(['ID', 'Title', 'Description', 'Company']);

        /** @var JobTo $item */
        foreach ($collection as $item) {
            $writer->insertOne(
                [
                    $item->getId(),
                    $item->getName(),
                    $item->getDescription(),
                    $item->getCompany(),
                ]
            );
        }

        return new Report($writer->getContent(), $this->mime());
    }

    /**
     * @return string
     */
    public function mime(): string
    {
        return 'application/csv';
    }

}