<?php

namespace App\Export\Formatter;

/**
 * Class FormatterRegistry
 *
 * @package App\Export\Formatter
 */
class FormatterRegistry
{
    /**
     * @var FormatterInterface[]
     */
    protected $formatters = [];

    /**
     * @param string             $format
     * @param FormatterInterface $formatter
     */
    public function register(string $format, FormatterInterface $formatter): void
    {
        $this->formatters[$format] = $formatter;
    }

    /**
     * @return FormatterInterface[]
     */
    public function all(): array
    {
        return $this->formatters;
    }

    /**
     * @return array
     */
    public function available(): array
    {
        return array_keys($this->formatters);
    }
}