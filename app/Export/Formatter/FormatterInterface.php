<?php

namespace App\Export\Formatter;

use App\Export\Dto\JobsCollection;
use App\Export\Dto\Report;

/**
 * Interface FormatterInterface
 *
 * @package App\Export\Formatter
 */
interface FormatterInterface
{
    /**
     * @TODO: Potential improvement, to make it return streams for large files
     *
     * @param JobsCollection $collection
     *
     * @return Report
     */
    public function format(JobsCollection $collection): Report;

    /**
     * @return string
     */
    public function mime(): string;
}