<?php

namespace App\Export\Formatter;

use App\Export\Dto\JobTo;
use Illuminate\Support\Str;
use Symfony\Component\Serializer\Serializer;

/**
 * Class XmlCutFormatter
 *
 * @package App\Export\Formatter
 */
class XmlCutFormatter extends XmlFormatter
{
    /**
     * @var int
     */
    protected $maxLength;

    /**
     * XmlCutFormatter constructor.
     *
     * @param Serializer $serializer
     * @param int        $maxLength
     */
    public function __construct(Serializer $serializer, int $maxLength = 100)
    {
        parent::__construct($serializer);

        $this->maxLength = $maxLength;
    }

    /**
     * @param JobTo $job
     *
     * @return JobTo
     */
    protected function prepare(JobTo $job): JobTo
    {
        $description = $job->getDescription();

        if (Str::length($description) > $this->maxLength) {
            $description = sprintf('%s ...', Str::substr($description, 0, $this->maxLength - 4));
        }

        return new JobTo(
            $job->getId(),
            $job->getName(),
            $description,
            $job->getCompany()
        );
    }

}