<?php

namespace App\Export\Formatter;

use App\Export\Dto\JobsCollection;
use App\Export\Dto\JobTo;
use App\Export\Dto\Report;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Serializer;

/**
 * Class XmlFormatter
 *
 * @package App\Export\Formatter
 */
class XmlFormatter implements FormatterInterface
{
    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * XmlFormatter constructor.
     *
     * @param Serializer $serializer
     */
    public function __construct(Serializer $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @TODO: Potential improvement, to make it return streams for large files
     *
     * @param JobsCollection $collection
     *
     * @return Report
     */
    public function format(JobsCollection $collection): Report
    {
        $data = $this->serializer->serialize(
            [
                'job' => collect($collection)->map(function (JobTo $job) {
                    return $this->prepare($job);
                })->all(),
            ],
            XmlEncoder::FORMAT
        );

        return new Report($data, $this->mime());
    }

    /**
     * @param JobTo $job
     *
     * @return JobTo
     */
    protected function prepare(JobTo $job): JobTo
    {
        return $job;
    }

    /**
     * @return string
     */
    public function mime(): string
    {
        return 'application/xml';
    }

}