<?php

namespace App\Refactoring;

use InvalidArgumentException;

class Calculator
{
    /**
     * @param float $a
     * @param float $b
     *
     * @return float
     */
    public function plus(float $a, float $b): float
    {
        return $a + $b;
    }

    /**
     * @param float $a
     * @param float $b
     *
     * @return float
     */
    public function minus(float $a, float $b): float
    {
        return $a - $b;
    }

    /**
     * Alias for minus() method
     *
     * @param float $a
     * @param float $b
     *
     * @return float
     */
    public function subtract(float $a, float $b): float
    {
        return $this->minus($a, $b);
    }

    /**
     * @param float $a
     *
     * @return float
     */
    public function squareRoot(float $a): float
    {
        return sqrt($a);
    }

    /**
     * @param float $a
     * @param float $b
     *
     * @return float|int
     */
    public function divide(float $a, float $b): float
    {
        if ($b === 0) {
            throw new InvalidArgumentException('Division by zero is impossible');
        }

        return $a / $b;
    }

    /**
     * @param float $a
     * @param float $b
     *
     * @return float|int
     */
    public function multiply(float $a, float $b)
    {
        return $a * $b;
    }
}