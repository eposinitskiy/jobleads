<?php

namespace App\Refactoring;

/**
 * Class StringTools
 *
 * @package App\Refactoring
 */
class StringTools
{
    /**
     * Concatenates given number of strings
     *
     * @param string ...$strings
     *
     * @return string
     */
    public static function concat(string ...$strings): string
    {
        return implode('', $strings);
    }

    /**
     * @param string $string
     */
    public static function writeLn(string $string): void
    {
        echo $string . PHP_EOL;
    }

    /**
     * @param string $string
     *
     * @return string
     */
    public static function upperCase(string $string): string
    {
        return strtoupper($string);
    }

    /**
     * @param string $string
     *
     * @return string
     */
    public static function lowerCase(string $string): string
    {
        return strtolower($string);
    }

    /**
     * @param string $string
     * @param string $algo Hashing algorithm.
     *                     For supported algorithms @see http://php.net/manual/ru/function.hash-algos.php
     *
     * @return string
     */
    public static function hash(string $string, string $algo = 'md5'): string
    {
        $supported = hash_algos();

        if (!\in_array(static::lowerCase($algo), $supported, true)) {
            throw new \OutOfBoundsException(
                sprintf('Algorithm [%s] is not supported, use on of [%s]', $algo, implode(', ', $supported))
            );
        }

        return hash($algo, $string);
    }

    /**
     * @param string $string
     *
     * @return string
     */
    public static function md5(string $string): string
    {
        return static::hash($string, 'md5');
    }

    /**
     * @param string $string
     *
     * @return mixed
     */
    public static function sha512(string $string): string
    {
        return static::hash($string, 'sha512');
    }

    /**
     * @param string $search
     * @param string $replace
     * @param string $subject
     *
     * @return string
     */
    public static function replace(string $search, string $replace, string $subject): string
    {
        return str_replace($search, $replace, $subject);
    }


    /**
     * @param string $string
     * @param string $charlist
     *
     * @return string
     */
    public static function trim(string $string, ?string $charlist = null): string
    {
        return $charlist ? trim($string, $charlist) : trim($string);
    }
}
