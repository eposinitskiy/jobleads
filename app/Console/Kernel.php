<?php

namespace App\Console;

use App\Console\Command\CountryTaxReportCommand;
use App\Console\Command\StudentsTaskCommand;
use App\Console\Command\ExportTaskCommand;
use App\Console\Command\TextFilesTaskCommand;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CountryTaxReportCommand::class,
        TextFilesTaskCommand::class,
        StudentsTaskCommand::class,
        ExportTaskCommand::class,
    ];
}
