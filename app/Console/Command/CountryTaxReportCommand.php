<?php

namespace App\Console\Command;

use App\TaxManager\Report\CountryReport;
use App\TaxManager\Repository\StateRepository;
use Illuminate\Console\Command;

/**
 * Class CountryTaxReportCommand
 *
 * @package App\Console\Command
 */
class CountryTaxReportCommand extends Command
{
    protected $signature = 'report:taxes';

    public function handle(StateRepository $repository, CountryReport $report)
    {
        foreach ($repository->states() as $state) {
            $report->add($state);
        }

        $this->comment('Per state report');
        $this->table(
            [
                'State',
                'Overall amount of taxes collected',
                'Average amount of taxes collected',
                'Average county tax rate',
            ],
            $this->perStateReport($report)
        );

        $this->comment('Average tax rate of the country');
        $this->info($report->avgRate());

        $this->comment('Collected overall taxes of the country');
        $this->info($report->totalIncome());
    }

    protected function perStateReport(CountryReport $report): array
    {
        $income    = $report->statesIncome();
        $avgIncome = $report->statesAvgIncome();
        $rate      = $report->statesAvgRate();
        $result    = [];

        foreach ($income as $state => $value) {
            $result[] = [
                $state,
                $value,
                $avgIncome[$state] ?? 0,
                $rate[$state] ?? 0,
            ];
        }

        return $result;
    }
}