<?php

namespace App\Console\Command;

use App\Export\Criteria\ExportCriteria;
use App\Export\Report\ExportManager;
use Illuminate\Console\Command;

/**
 * Class TestCommand
 *
 * @package App\Console\Command
 */
class ExportTaskCommand extends Command
{
    protected $signature = 'task:export {--f|format=csv : Export Format}';

    protected $description = 'Development / Task 3 [ETA: ~3hr]';

    public function handle(ExportManager $manager)
    {
        $data = $manager->withCriteria(ExportCriteria::fromArray(['limit' => 5]))
                        ->getReport($this->option('format') ?? 'csv');

        dump($data);
    }
}