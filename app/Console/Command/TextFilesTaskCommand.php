<?php

namespace App\Console\Command;

use Illuminate\Console\Command;

/**
 * Class TextFilesTaskCommand
 *
 * @package App\Console\Command
 */
class TextFilesTaskCommand extends Command
{
    protected $signature = 'task:text-files-task';

    protected $description = 'Development / Task 1 [ETA: 15 min]';

    public function handle()
    {
        $txtNumbers = $this->grabTxtFileContent();
        $csvNumbers = $this->grabCsvFileContent();

        $merged       = $sorted = array_merge($txtNumbers, $csvNumbers);
        $intersection = array_intersect($txtNumbers, $csvNumbers);
        sort($sorted);

        $this->info('Elements of `txt` file then elements of `csv` file');
        dump($merged);

        $this->info('Elements of `txt` and `csv` files in ascending order');
        dump($sorted);

        $this->info('Intersection of elements in both files');
        dump($intersection);
    }

    /**
     * @TODO: Potential improvements: Use <abstract>[file reader object] that could handle file reading with filters applied
     *
     * @return array
     */
    protected function grabTxtFileContent(): array
    {
        $path = resource_path('code-test/development/task-1/file1.txt');

        return $this->filter(file($path));
    }

    protected function grabCsvFileContent(): array
    {
        $path = resource_path('code-test/development/task-1/file2.csv');

        return $this->filter((new \SplFileObject($path))->fgetcsv());
    }

    protected function filter(array $input): array
    {
        return collect($input)->map(function ($num) {
            return (int)trim($num);
        })->all();
    }
}