<?php

namespace App\Console\Command;

use App\Students\Dao\ScoreDao;
use App\Students\Subject\StudentSubject;
use App\Students\Subject\StudyClassSubject;
use Illuminate\Console\Command;

/**
 * Class StudentsTaskCommand
 *
 * @package App\Console\Command
 */
class StudentsTaskCommand extends Command
{
    protected $signature = 'task:students';

    protected $description = 'Development / Task 2 [ETA: 1 hr 15 min]';

    public function handle(ScoreDao $dao)
    {
        $this->showStudentScores($dao);
        $this->showClassesScores($dao);
        $this->showOverallAverageGrade($dao);
    }

    protected function showClassesScores(ScoreDao $dao): void
    {
        $scores = [];
        /** @var StudyClassSubject $studyClass */
        foreach ($dao->allClasses() as $studyClass) {
            $scores[] = [
                $studyClass->getName(),
                $studyClass->avgScore(),
            ];
        }

        $this->info('The average grade of all students for each class');
        $this->table(
            ['Class name', 'Average score'],
            $scores
        );
    }

    protected function showStudentScores(ScoreDao $dao): void
    {
        $scores = [];
        foreach ($dao->allStudents() as $student) {
            $scores[] = [
                sprintf('%s %s', $student->getStudent()->getFirstName(), $student->getStudent()->getLastName()),
                $student->avgScore(),
            ];
        }

        $this->info('A list of students with their personal average grade');
        $this->table(
            ['Student', 'Average score'],
            $scores
        );
    }

    protected function showOverallAverageGrade(ScoreDao $dao): void
    {
        $students = collect($dao->allStudents());

        $this->info('The overall average grade');
        $this->line($students->sum(function (StudentSubject $subject) {
                        return $subject->avgScore();
                    }) / $students->count());
    }
}