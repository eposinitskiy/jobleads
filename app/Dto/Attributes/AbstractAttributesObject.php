<?php

namespace App\Dto\Attributes;

use Illuminate\Support\Str;

/**
 * Class AbstractValueObject
 *
 * @package App\Dto
 */
abstract class AbstractAttributesObject
{
    /**
     * @var array
     */
    protected $data = [];

    /**
     * @var array
     */
    protected $allowed = [];

    /**
     * AbstractAttributesObject constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->fill($data);
    }

    /**
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        if (!Str::startsWith($name, 'with')) {
            throw new \BadMethodCallException(sprintf('Method %s is not allowed', $name));
        }

        $original  = null;
        $attribute = lcfirst(Str::replaceFirst('with', '', $name));

        if (!$this->attributeIsAllowed($attribute)) {
            $original  = $attribute;
            $attribute = Str::snake($attribute);
        }

        if (!$this->attributeIsAllowed($attribute)) {
            throw new \OutOfBoundsException(
                sprintf('Attribute [%s] or [%s] does not exist', $original, $attribute)
            );
        }

        return \call_user_func_array([$this, 'with'], array_merge([$attribute], (array)$arguments));
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        return $this->get($name);
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function __isset($name)
    {
        return $this->attributeIsAllowed($name) ? isset($this->data[$name]) : false;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->data;
    }

    /**
     * @param array $data
     *
     * @return static
     */
    public static function fromArray(array $data)
    {
        return new static($data);
    }

    /**
     * @param array $data
     */
    protected function fill(array $data)
    {
        foreach ($data as $key => $value) {
            if (!$this->attributeIsAllowed($key)) {
                continue;
            }

            $this->setValue($key, $value);
        }
    }

    /**
     * @param string $attribute
     * @param mixed  $value
     *
     * @return $this
     */
    protected function with($attribute, $value)
    {
        return (clone $this)->setValue($attribute, $value);
    }

    /**
     * @param string $attribute
     * @param mixed  $default
     *
     * @return mixed
     */
    protected function get($attribute, $default = null)
    {
        if (!$this->attributeIsAllowed($attribute)) {
            throw new \OutOfBoundsException(
                sprintf('Attribute %s does not exist', $attribute)
            );
        }

        return $this->data[$attribute] ?? $default;
    }

    /**
     * @param string $attribute
     * @param string $value
     *
     * @return AbstractAttributesObject
     */
    protected function setValue($attribute, $value)
    {
        if ($filter = $this->guessFilterMethod($attribute)) {
            $value = $this->$filter($value);
        }

        $this->data[$attribute] = $value;

        return $this;
    }

    /**
     * @param string $attribute
     *
     * @return bool|string
     */
    protected function guessFilterMethod($attribute)
    {
        $possibleSuffixes = [
            $attribute,
            Str::camel($attribute),
        ];

        foreach ($possibleSuffixes as $possibleSuffix) {
            $filterMethod = 'filter' . ucfirst($possibleSuffix);

            if (method_exists($this, $filterMethod)) {
                return $filterMethod;
            }
        }

        return false;
    }

    /**
     * @param string $attribute
     *
     * @return bool
     */
    protected function attributeIsAllowed($attribute): bool
    {
        if (empty($this->allowed)) {
            $this->allowed = array_flip(static::attributes());
        }

        return array_key_exists($attribute, $this->allowed);
    }

    /**
     * Returns available attribute names
     *
     * @return array
     */
    abstract public static function attributes(): array;
}
