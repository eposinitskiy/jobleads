## Jobleads code test

### Development test (Backend)

Run and test Country taxes calculator (*Docker + Docker Compose required*):

1. Clone this project: `git clone git@gitlab.com:eposinitskiy/jobleads.git`
2. Within a project root directory run `docker-compose up -d` 
to get project up and running (Give ~1 minute on first boot to initiate the DB with dump)
3. Within a project root directory run `.docker/report`. This will run a CLI command to generate report

Code test sources could be found in `App/TaxManager` namespace

### Refactoring test (Backend)

Refactored version could be found at `App\Refactoring\StringTools`

### Frontend test

All the frontend sources could be found at `reources/client`

Tech stack:

- SCSS
- VueJS framework
- VueX state managements for VueJS
- Glyphicons
- TypeScript
- Webpack (v.3)

Landing page could be found at `public/landing.html`. Assets already compiled.

If you want to build project (re-compile assets) run `.docker/build` command 
